// the main method for the game basically

#include "../include/game.h"

int main()
{
    Game::Start();

    return 0;
}